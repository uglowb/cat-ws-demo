# cat-ws-demo
> A NodeJS demo of how you might call a web service, transform some data then render it

## Requirements
- Node 6+

## Installation
```bash
npm i
```

## Usage
```bash
npm start
```

## Design Decisions
- I opted not to do this for the browser because of my desire to write it using ES6 modules. Since ES6 modules are not natively supported in every browser, 
  it would require the use of WebPack and other tooling which just adds noise.
- I decided not to introduce a Cat data type because the problem domain is tiny.