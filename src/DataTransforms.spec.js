const assert = require('assert');
const transforms = require('./DataTransforms');
const testData = require('../fixtures/cats.json');

describe('DataTransforms', () => {
  it('should have the correct interface', () => {
    assert.equal(typeof transforms, 'object');
    assert.equal(typeof transforms.groupByFn, 'function');
    assert.equal(typeof transforms.filterSortCats, 'function');
    assert.equal(typeof transforms.getCatsByOwnerGender, 'function');
  });

  describe('groupByFn', () => {
    it('should return a function which groups data by the supplied key', () => {
      let result = transforms.groupByFn('gender')(testData);
      assert.deepEqual(Object.keys(result), ['Male', 'Female']);
    });
  });

  describe('filterSortCats', () => {
    it('should sort data by the keys', () => {
      let result = transforms.filterSortCats(transforms.groupByFn('gender')(testData));
      assert.deepEqual(result.Female, ['Alice', 'Jennifer', 'Samantha']);
      assert.deepEqual(result.Male, ['Bob', 'Fred', 'Steve']);
    });
  });

  describe('getCatsByOwnerGender', () => {
    it('should return the cats grouped by gender, sorted by cat name', () => {
      let result = transforms.getCatsByOwnerGender(testData);
      assert.deepEqual(result.Female, ['Alice', 'Jennifer', 'Samantha']);
      assert.deepEqual(result.Male, ['Bob', 'Fred', 'Steve']);
    });
  });
});
