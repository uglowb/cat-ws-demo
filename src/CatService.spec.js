const assert = require('assert');
const CatService = require('./CatService');
const mockHttp = require('nock');

describe('CatService', () => {
  it('should have the correct interface', () => {
    let service = new CatService({});
    assert.equal(typeof service, 'object');
    assert.equal(typeof service.getCats, 'function');
  });

  describe('getCats()', () => {
    function mockHttpResponse(code, fixture) {
      mockHttp('http://example.com')
        .get('/cats')
        .replyWithFile(code, `${__dirname}/../fixtures/${fixture}`);
    }

    function customHttpMock(url) {
      return new Promise((resolve) => {
        let response = {
          ok: true,
          json: () => url,
        };
        return resolve(response);
      });
    }

    afterEach(() => {
      mockHttp.cleanAll();
    });

    it('should have a default CatService url', () => {
      let service = new CatService({httpRequestor: customHttpMock});
      return service.getCats().then((url) => {
        assert.equal(url, 'http://bit.ly/2uK2P03');
      });
    });

    it('should return an array of cat objects via a promise when the response is successful', () => {
      mockHttpResponse(200, 'cats.json');
      let service = new CatService({url: 'http://example.com/cats'});
      return service.getCats().then((data) => {
        assert.equal(data.length, 6);
      });
    });

    it('should return an empty array via a promise when the data is not JSON', () => {
      mockHttpResponse(200, '404.txt');
      let service = new CatService({url: 'http://example.com/cats'});
      return service.getCats().then((data) => {
        assert.equal(data.length, 0);
      });
    });

    it('should return an empty array via a promise when the response is unsuccessful', () => {
      mockHttpResponse(403, '404.txt');
      let service = new CatService({url: 'http://example.com/cats'});
      return service.getCats().then((data) => {
        assert.equal(data.length, 0);
      });
    });
  });
});
