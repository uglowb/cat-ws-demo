/**
 * Use a functional-programming style for dealing with the data
 */

const groupBy = require('lodash.groupby');
const pipe = (...fns) => (x) => fns.reduce((v, f) => f(v), x);    // Creates a function that applies a series of functions to some input data

function groupByFn(groupByIteratee) {
  return (data) => groupBy(data, groupByIteratee);
}

function filterSortCats(data) {
  Object.keys(data).forEach((key) => {
    data[key] = data[key].map((cat) => cat.name).sort();
  });
  return data;
}


function getCatsByOwnerGender(data) {
  return pipe(groupByFn('gender'), filterSortCats)(data);
}

module.exports = {
  filterSortCats,
  getCatsByOwnerGender,
  groupByFn,
};
