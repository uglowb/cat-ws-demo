console.log('Cat Web Service Demo');

const CatService = (require('./CatService'));
const transform = require('./DataTransforms').getCatsByOwnerGender;
const renderer = require('./RenderFactory').createTextRenderer(console.log);

// Do all the things
function run(serviceEndPoint) {
  const service = new CatService({url: serviceEndPoint});
  return service.getCats().then(transform).then(renderer);
}

module.exports = {
  run
};


// Run the command when called from the command line (but not when in a test environment)
/* istanbul ignore if */
if (process.env.NODE_ENV !== 'test') {
  return run();
}
