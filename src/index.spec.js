const assert = require('assert');
const demo = require('./index');
const stdout = require('test-console').stdout;

describe('Node', () => {
  it('should run the demo and return the text-version of the result', () => {
    let inspect = stdout.inspect();

    return demo.run('http://bit.ly/2uK2P03').then(() => {
      inspect.restore();

      // This result is heavily dependent on the service not changing
      assert.equal(inspect.output[0], 'Male\n * Bob\n * Fred\n * Steve\nFemale\n * Alice\n * Jennifer\n * Samantha\n\n');
    });
  });
});
