const assert = require('assert');
const factory = require('./RenderFactory');

const testData = {
  foo: ['fee', 'fi', 'fo', 'fum'],
  car: 'lah',
  bar: ['bee', 'bi', 'bo', 'bum'],
};

describe('RenderFactory', () => {
  it('should have the correct interface', () => {
    assert.equal(typeof factory, 'object');
    assert.equal(typeof factory.createTextRenderer, 'function');
    assert.equal(typeof factory.createHTMLRenderer, 'function');
  });

  describe('createTextRenderer()', () => {
    it('should produce a text version of the input data structure', () => {
      let result;
      let callback = (data) => result = data;
      let renderer = factory.createTextRenderer(callback);
      renderer(testData);

      assert.equal(result, 'foo\n * fee\n * fi\n * fo\n * fum\ncar\nbar\n * bee\n * bi\n * bo\n * bum\n');
    });
  });

  describe('createHTMLRenderer()', () => {
    it('should produce a HTML version of the input data structure', () => {
      let mockElem = {
        innerHTML: '',
      };
      let renderer = factory.createHTMLRenderer(mockElem);
      renderer(testData);

      assert.equal(mockElem.innerHTML, '<strong>foo</strong><ul><li>fee</li><li>fi</li><li>fo</li><li>fum</li></ul>' +
        '<strong>car</strong>' +
        '<strong>bar</strong><ul><li>bee</li><li>bi</li><li>bo</li><li>bum</li></ul>');
    });
  });
});
