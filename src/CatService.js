require('isomorphic-fetch');  // Adds 'fetch()' to the global object or the window object

class CatService {
  constructor({httpRequestor = fetch, url = 'http://bit.ly/2uK2P03'}) {
    this.httpRequestor = httpRequestor;
    this.url = url;
  }
  getCats() {
    return this.httpRequestor(this.url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok.');
      })
      .catch((err) => {
        console.error(err);
        return [];
      });
  }
}

module.exports = CatService;
