/**
 * This is not recursive. But we know could make it recursive if necessary.
 * @param {Object} data - a 2-level data structure
 * @param {function} parentFn - how to render a parent element
 * @param {function} containerFn - how to render a container element
 * @param {function} childFn - how to render a child element
 * @return {string} - string version of the tree structure
 */
function renderTree(data, parentFn, containerFn, childFn) {
  let response = '';

  Object.keys(data).forEach((parent) => {
    // Assume data is tree shape
    response += parentFn(parent);

    if (typeof data[parent].reduce === 'function') {   // duck typing for 'Array'
      let children = data[parent].reduce((acc, child) => {
        acc += childFn(child);
        return acc;
      }, '');
      response += containerFn(children);
    }
  });

  return response;
}


class RenderFactory {

  createTextRenderer(writeFn) {
    return (data) => {
      return writeFn(renderTree(data, (parent) => `${parent}\n`, (children) => children, (child) => ` * ${child}\n`));
    };
  }

  createHTMLRenderer(domElem) {
    return (data) => domElem.innerHTML = renderTree(data, (parent) => `<strong>${parent}</strong>`, (children) => `<ul>${children}</ul>`, (child) => `<li>${child}</li>`);
  }
}

module.exports = new RenderFactory();
